#include <stdbool.h>
#include <stdio.h>
#include <string.h>

// ---------- //
// Constantes //
// ---------- //

#define CARACTERE_VIDE '-'
#define HAUTEUR_MAX 15
#define LARGEUR_MAX 20

// --------------------- //
// Structures de donnees //
// --------------------- //

struct TabHauteurs {
    unsigned int largeur;
    int contenu[LARGEUR_MAX + 1];
};

struct Montagne {
    unsigned int hauteur;
    unsigned int largeur;
    char contenu[LARGEUR_MAX][HAUTEUR_MAX];
};

// ---------- //
// Prototypes
// ---------- //
void retirEau(char *eau, struct Montagne *montagne);
bool largeurValide(int largeur);
bool sontInt(char *s);
bool terreEauDifferents(char *t, char *e);
bool nbArgsValide(int argc);
bool estCaractereUnique(char *s);
bool initTabHauteurs(char *s, struct TabHauteurs  *tabRet);
unsigned int obtenirHauteurMax(struct TabHauteurs *hauteurs);
bool calculerHauteurs(char *s, struct TabHauteurs *hauteurs);
void construireMontagne(char *terre, struct TabHauteurs *hauteurs,
        struct Montagne *montagne);
void ajouterEau(char *terre, char *eau, struct Montagne *montagne);
void afficherMontagne(struct Montagne *montagne);

int main(int argc, char* argv[]){

    struct TabHauteurs tabRet;
    struct Montagne mont;

    if(nbArgsValide(argc) && estCaractereUnique(argv[1]) && estCaractereUnique(argv[2])
            && terreEauDifferents(argv[1], argv[2]) && sontInt(argv[3]) && initTabHauteurs(argv[3], &tabRet))
    {

        construireMontagne(argv[1], &tabRet, &mont);
        ajouterEau(argv[1],argv[2], &mont);
        afficherMontagne(&mont);

        return 0;

    }

    return 1;

}

/**
 * Affiche une representation graphique de la montagne remplie d'eau sur la console. Celle-ci est basee sur le contenu de la structure Montagne passee en parametre.
 *
 * @param *montagne une structure contenant les valeurs a afficher.
 */
void afficherMontagne(struct Montagne *montagne){

    unsigned int x = 0;
    int y = montagne->hauteur-1;

    while(y >= 0){

        while(x < montagne->largeur){

            printf("%c", montagne->contenu[x][y]);
            x++;

        }

        printf("\n");
        x = 0;
        y--;    

    }

}

/**
 * Enleve les caracteres d'eau de trop ajoutes apres le dernier caractere de terre par la methode ajouterEau.
 *
 * @param *eau Une chaine dont le premier caractere represente l'eau.
 * @param montagne la structure qui contient les valeurs representant la montagne.
 */
void retirEau(char *eau, struct Montagne *montagne){

    unsigned int x = montagne->largeur-1;
    int y = montagne->hauteur-1;

    while(y >= 0){

        while(montagne->contenu[x][y] == eau[0]){

            montagne->contenu[x][y] =  CARACTERE_VIDE;

            x--;

        }

        x = montagne->largeur-1;
        y--;    

    }
}
/**
 * Ajoute les caracteres d'eau dans les espaces vides entre deux sommets de terre.
 *
 * @param *terre Une chaine dont le premier caractere represente la terre qui constitue la montagne.
 * @param *eau Une chaine dont le premier caractere represente l'eau.
 * @param *montagne La structure qui contient les valeurs representant la montagne.
 */
void ajouterEau(char *terre, char *eau, struct Montagne *montagne){

    unsigned int x = 0;
    unsigned int y = 0;

    while(x != montagne->largeur && y != montagne->hauteur){

        while(x+1 < montagne->largeur){


            if((montagne->contenu[x][y] == terre[0] || montagne->contenu [x][y] == eau[0])  && montagne->contenu[x+1][y] != terre[0]){

                montagne->contenu[x+1][y] = eau[0];

            }  




            x++;

        }

        x = 0;
        y++;

    }

    retirEau(eau, montagne);

}
/**
 * Remplis la structure passee en argument de caractere(s) terre de facon a representer la montagne dont les hauteurs sont contenues dans la structure hauteurs.
 *
 * @param *terre Chaine dont le premier caractere representera la terre constituant la montagne.
 * @param *hauteurs structure contenant les valeurs desirees pour la montagne et sa largeur.
 * @param *montagne structure dans laquelle sera placee une representation graphique de la montagne a affichier.
 */
void construireMontagne(char *terre, struct TabHauteurs *hauteurs,
        struct Montagne *montagne){

    montagne->largeur = hauteurs->largeur;
    montagne->hauteur = obtenirHauteurMax(hauteurs);
    unsigned int x = 0;
    unsigned int y = 0;

    while(x != montagne->largeur && y != montagne->hauteur){

        while(x < montagne->largeur){

            if(y < hauteurs->contenu[x]){

                montagne->contenu[x][y] = terre[0];

            }else{

                montagne->contenu[x][y] = '-';

            } 

            x++;

        }

        x = 0;
        y++;

    }


}

/**
 * Retourne la valeur maximum dans les hauteurs de la structure passee en argument
 *
 * @param *hauteurs structure contenant toutes les hauteurs desirees pour le montagne a afficher.
 * @return la plus grande valeur dans la liste de hateurs contenue dans la structure passee en argument.
 */
unsigned int obtenirHauteurMax(struct TabHauteurs *hauteurs){

    unsigned int i;
    unsigned int max = 0;

    for( i = 0; i < hauteurs->largeur; i++){

        if(hauteurs->contenu[i] > max){

            max = hauteurs->contenu[i];

        }    

    }

    return max;

}

/**
 * Remplis des valeurs separes par des virgules de la string s passee en argument et y ajoute le nombre d'entrees dans largeur. 
 *
 * Lance une erreur si une valeur trop grande ou trop petite est passee en argument ou qu'un trop grand nombre de valeurs est passe.
 *
 * Pour un bon fonctionnement, cette string ne doit pas contenir autre chose que des valeurs numeriques et des ','. Elle ne doit pas contenir de suite de ',' non-plus.
 *
 * @param *s string contenant les valeurs a ajouter en tant que hauteurs dans tab. 
 * @param *tab structure dans laquelle entrer les valeurs representant les hauteurs et la largeur
 * @return true si l'operation s'est bien deroulee, false sinon.
 */
bool initTabHauteurs(char *s, struct TabHauteurs *tab){

    unsigned int largeur = 0;    
    char tmp[3*LARGEUR_MAX];
    char *pc;

    strcpy(tmp, s);
    pc = strtok(tmp, ",");

    while (pc != NULL) {

        tab->contenu[largeur] = atoi(pc);
        if(tab->contenu[largeur] > HAUTEUR_MAX || 0 > tab->contenu[largeur] ){

            printf("Hauteur invalide: la hauteur doit etre un nombre entre 0 et %d\n", HAUTEUR_MAX);
            return false;

        }

        largeur++;
        pc = strtok(NULL, ",");
    }
    if(!largeurValide(largeur)){

        return false;

    }
    tab->largeur = largeur;

    return true;

}

/**
 * Verifie que la chaine passee en argument ne contient que des caracteres numeriques et des ','.
 *
 * @param *s chaine de caracteres a tester.
 * @return true si la chaine contient seulement des caracteres numeriques et des ',' et false dans le cas contraire.
 */
bool sontInt(char *s){

    int i = 0;

    while(s[i] != '\0'){

        if(s[i] != ','){



            if(isalpha(s[i])) {

                printf("Hauteur invalide: la hauteur doit etre un nombre entre 0 et %d\n", HAUTEUR_MAX); 
                return false;

            }
        }

        i++;

    }

    return true;

}
/**
 * Verifie que la largeur passee en argument est valide
 *
 * @param largeur largeur a valider.
 * @return true si largeur est valide, false sinon.
 */
bool largeurValide(int largeur){

    if(largeur > LARGEUR_MAX || largeur == 0 ){

        printf("Largeur invalide: le nombre de hauteurs doit etre entre 1 et %d\n", LARGEUR_MAX);
        return false;

    }

    return true;

}
/**
 * Verifie que les deux caracteres passes en argument son differents.
 *
 * @param *t caractere representant la terre.
 * @param *e caractere representant l'eau
 * @return true si les deux caracteres sont differents, false sinon.
 */
bool terreEauDifferents(char *t, char *e){

    if(t[0] != e[0]){
        return true;

    }

    printf("Les codes doivent etre distincts\n");
    return false;

}
/**
 * Verifie que le nombre d'arguments passes en appelant le programme est valide.
 *
 * @param argc valeur representant le nombre d'arguments passes a l'appel du programme(+1).
 * @return true si le nombre d'arguments passes est valide, false sinon
 */
bool nbArgsValide(int argc){

    if(argc == 4){

        return true;

    }

    printf("Nombre d'arguments invalides: il en faut 3\n");
    return false;

}
/**
 * Verifie que le string passe en argument contient seulement une caractere.
 *
 * @param *s string a valider.
 * @return true si l'argument a un seul caractere, false sinon.
 */
bool estCaractereUnique(char *s){

    if (s[1]!='\0'){

        printf("Code %s invalide: il doit etre un caractere unique\n", s);
        return false;

    }

    return true;

}


